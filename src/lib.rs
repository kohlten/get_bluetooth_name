cfg_if::cfg_if! {
    if #[cfg(unix)] {
        mod unix;

        pub use unix::NameGetter;
    } else if #[cfg(windows)] {
        mod windows;

        pub use windows::NameGetter;
    } else {
        compile_error!("get_bluetooth_name doesn't support this os yet");
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
