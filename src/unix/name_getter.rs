use std::{io, ptr};
use std::os::raw::c_int;
use libbluetooth::hci_lib::{hci_get_route, hci_open_dev, hci_close_dev, hci_read_remote_name};
use libbluetooth::bluetooth::bdaddr_t;

const NAME_MAX_SIZE: i32 = 248;
const TIMEOUT: c_int = 4;

pub struct NameGetter {
    local_socket: i32,
}

impl NameGetter {
    pub fn new() -> io::Result<Self> {
        // Get the bluetooth device id
        let device_id = unsafe { hci_get_route(ptr::null_mut()) };
        if device_id == -1 {
            return Err(io::Error::last_os_error());
        }

        // Open a socket to the bluetooth device
        let local_socket = unsafe { hci_open_dev(device_id) };
        if local_socket == -1 {
            return Err(io::Error::last_os_error());
        }
        Ok(Self { local_socket })
    }

    pub fn get_name(&self, addr: [u8; 6]) -> io::Result<String> {
        let bt_addr = bdaddr_t { b: addr };
        let mut str_name: Vec<i8> = Vec::new();

        str_name.resize(NAME_MAX_SIZE as usize, 0);

        // Read the name of the bluetooth device
        if -1 == unsafe { hci_read_remote_name(self.local_socket, &bt_addr, NAME_MAX_SIZE, str_name.as_mut_ptr(), TIMEOUT) } {
            return Err(io::Error::last_os_error());
        }

        // // Get the name from the returned value. The value is 248 long with a zero at the end of the string.
        for i in 0..str_name.len() {
            if str_name[i] == 0 {
                str_name.resize(i, 0);
                break;
            }
        }
        Ok(String::from_utf8(str_name.iter().map(|&c| { c as u8 }).collect()).unwrap())
    }
}

impl Drop for NameGetter {
    fn drop(&mut self) {
        if -1 == unsafe { hci_close_dev(self.local_socket) } {
            panic!("{}", io::Error::last_os_error())
        }
    }
}

fn get_name(addr: [u8; 6]) -> io::Result<String> {
    // Get the bluetooth device id
    let device_id = unsafe { hci_get_route(ptr::null_mut()) };
    if device_id == -1 {
        return Err(io::Error::last_os_error());
    }

    // Open a socket to the bluetooth device
    let local_socket = unsafe { hci_open_dev(device_id) };
    if local_socket == -1 {
        return Err(io::Error::last_os_error());
    }

    let bt_addr = bdaddr_t { b: addr };
    let mut str_name: Vec<i8> = Vec::new();

    str_name.resize(NAME_MAX_SIZE as usize, 0);

    // Read the name of the bluetooth device
    if -1 == unsafe { hci_read_remote_name(local_socket, &bt_addr, NAME_MAX_SIZE, str_name.as_mut_ptr(), TIMEOUT) } {
        return Err(io::Error::last_os_error());
    }

    // // Get the name from the returned value. The value is 248 long with a zero at the end of the string.
    for i in 0..str_name.len() {
        if str_name[i] == 0 {
            str_name.resize(i, 0);
            break;
        }
    }

    if -1 == unsafe { hci_close_dev(local_socket) } {
        panic!("{}", io::Error::last_os_error())
    }
    Ok(String::from_utf8(str_name.iter().map(|&c| { c as u8 }).collect()).unwrap())
}