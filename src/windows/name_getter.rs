use std::{io, mem, ptr};
use winapi::um::bluetoothapis::{BLUETOOTH_FIND_RADIO_PARAMS, BluetoothFindFirstRadio, BLUETOOTH_ADDRESS, BluetoothFindRadioClose, BLUETOOTH_DEVICE_INFO, BluetoothGetDeviceInfo};
use winapi::um::winnt::HANDLE;
use winapi::um::winsock2::WSAGetLastError;
use winapi::ctypes::c_void;
use std::mem::transmute;

pub struct NameGetter {
    radio: *mut c_void
}

impl NameGetter {
    pub fn new() -> io::Result<Self> {
        let mut params = BLUETOOTH_FIND_RADIO_PARAMS::default();
        params.dwSize = mem::size_of::<BLUETOOTH_FIND_RADIO_PARAMS>() as u32;

        // Find the bluetooth radio using the params
        let mut handle: HANDLE = ptr::null_mut();
        let radio = unsafe { BluetoothFindFirstRadio(&params, &mut handle) };
        if radio.is_null() {
            return Err(last_error());
        }

        Ok(Self { radio })
    }

    pub fn get_name(&self, addr: [u8; 6]) -> io::Result<String> {
        // Get the name from the device using the radio
        let mut info = BLUETOOTH_DEVICE_INFO::default();
        info.Address = addr_to_int(addr);
        info.dwSize = mem::size_of::<BLUETOOTH_DEVICE_INFO>() as u32;
        if 0 != unsafe { BluetoothGetDeviceInfo(self.radio, &mut info) } {
            return Err(last_error());
        }

        // Get the name from the returned value. The value is 248 long with a zero at the end of the string.
        let mut name = info.szName.to_vec();
        for i in 0..name.len() {
            if name[i] == 0 {
                name.resize(i, 0);
                break;
            }
        }

        Ok(String::from_utf16(&name).unwrap())
    }
}

impl Drop for NameGetter {
    fn drop(&mut self) {
        // Close the open radio
        unsafe { BluetoothFindRadioClose(self.radio) };
    }
}

fn last_error() -> io::Error {
    io::Error::from_raw_os_error(unsafe { WSAGetLastError() })
}

fn addr_to_int(addr: [u8; 6]) -> BLUETOOTH_ADDRESS {
    unsafe { transmute::<[u8; 8], u64>([addr[0], addr[1], addr[2], addr[3], addr[4], addr[5], 0, 0]) }
}